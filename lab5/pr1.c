#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>        /* For mode constants */
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>

// ! se putea folosi functia atoi
int toInt(char * number) {
	int result = 0;
	int i=0;
	for(i=0; i<strlen(number); i++ ){
		 result = result*10 + (number[i]-'0');
	}
	return result;
}

char * toChar(int number) {
	char * result = (char *) malloc(5* sizeof(char));
	int i=0;
	while(number > 0) {
		result[i] = number % 10;
		printf("%d\n", number % 10);

		number = number /10;
		i++;
	}

	// char * result;
	 // snprintf(result, 5, "%d", number);

	return result;
}

int main(int argc, char *argv[]) {
	// printf("%s\n", toChar(512));
	if(argc < 2) {
		printf("Error! Wrong number of parameters. Exit\n");
		return 0;
	}

	printf("Starting parent %d\n",getpid());

	char shm_name[] = "myshm";
	int shm_fd;

	shm_fd = shm_open(shm_name, O_CREAT|O_RDWR, S_IRUSR|S_IWUSR);
	if(shm_fd < 0) {
		perror(NULL);
		return errno;
	}
	else {
		printf("Shared memory open\n");
	}

	// Definim dimensiunea zonei de memorie
	size_t shm_size = 1000 * getpagesize();

	if(ftruncate(shm_fd, shm_size) == -1) {
		perror(NULL);
		shm_unlink(shm_name);
		return errno;	
	}


	// MALLOC??
	char * input_nr_char;
	pid_t pid[100];
	// char * child_nr = (char *) malloc(5 * sizeof(char));
	char child_nr[5];
	char char_shm_fd[5];
	snprintf(char_shm_fd, 5, "%d", shm_fd);
		
	for(int i=0; i<argc-1; i++) {
 		pid[i] = fork();

		input_nr_char = argv[i+1];
		printf("Input nr:%s\n", input_nr_char);
		// i is the child number
		// itoa(i, child_nr, 10);
		
		// child_nr = toChar(i);
		snprintf(child_nr, 5, "%d", i);
		char *childargv[] = {"", input_nr_char, char_shm_fd, child_nr, NULL};
		
 		if(pid[i]<0) {
			return errno;
		} else if(pid[i]==0) {
			printf("My PID=%d Child PID=%d\n", getppid(), getpid());
			execve("./calculate", childargv, NULL);
			
			perror(NULL);
			printf("Parent %d Me %d\n", getppid(), getpid());

		} 
	}


	for(int i=0; i<argc-1; i++) {
		pid_t child_pid = wait(NULL);
		if(child_pid < 0){
			perror(NULL);
		}
		else {
			printf("Done parent %d Me %d \n", getpid(), child_pid);
		}
	}	

	// char * shm_ptr = (char*) malloc(1000);
	char * shm_ptr; 
	// toata zona de memorie se incarca in spatiul procesului parinte
	// PROT_READ => doar pt citire
	shm_ptr = mmap(0, getpagesize()*1000, PROT_READ, MAP_SHARED, shm_fd, 0);
	// shm_ptr = mmap(0, getpagesize()*1000, PROT_WRITE, MAP_SHARED, shm_fd, 0);
	if(shm_ptr==MAP_FAILED) {
		perror(NULL);
		shm_unlink(shm_name);
		return errno;
	}
	// shm_ptr = "slsdflsdf";

	printf("Shm ptr: %s\n", shm_ptr);
	// Unmap toata zona de memoriesu
	munmap(shm_ptr,getpagesize()*1000);

	

	return 0;
}