#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>


int main() 
{
	char * stringToWrite = "Hello world\n";
	int writtenBytes = 1;

	while(writtenBytes <  strlen(stringToWrite)) {
		writtenBytes = write(1, stringToWrite, strlen(stringToWrite));

		if (writtenBytes < 0) {
  			perror("Error writting bytes");
  			return errno;
  		}	
	}
	
	return 0;
}
