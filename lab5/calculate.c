#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>        /* For mode constants */
#include <fcntl.h>
#include <stdlib.h>

int toInt(char * number) {
	int result = 0;
	int i=0;
	for(i=0; i<strlen(number); i++ ){
		 result = result*10 + (number[i]-'0');
	}
	return result;
}

int main(int argc, char *argv[]) {
	printf("Child execution:\n");

	// printf("argc: %d\n", argc);
	if(argc != 4) {
		printf("Error! Wrong number of parameters in calculate program. Exit\n");
		return 0;
	}

	int result[20];
	int cnt = 1;

	// read shared memory file descriptor from parent
	int shm_fd = toInt(argv[2]);
	// read child number from parent
	int child_nr = toInt(argv[3]);
	printf("child nr: %d\n", child_nr);

	//read input nr from parent
	int input_nr = toInt(argv[1]);
	printf("Input nr: %d: \n", input_nr);
	result[0] = input_nr;

	while(input_nr>1) {
		if(input_nr % 2 ==0) {
			input_nr = input_nr/2;
		}
		else {
			input_nr = 3 * input_nr + 1;
		}
		result[cnt] = input_nr;
		// printf("%d ", input_nr );
		cnt++;
	}

	char * shm_ptr;

	// // TODO: verificat daca dimensiunea vectorului nu iese din 
	// // dimensiunea memoriei partajate
	// // incarc memoria partajata in spatiul procesului
	shm_ptr = mmap(0, getpagesize(), PROT_WRITE, MAP_SHARED, shm_fd, child_nr*getpagesize());
	
	if(shm_ptr==MAP_FAILED) {
		perror(NULL);
		shm_unlink(shm_name);
		return errno;
	}
	
	// shm_ptr = "sdf";

	
	// printf("SHM PTR:%s\n", shm_ptr);
	sprintf(shm_ptr, "%d", 4);
	// // de folosit sprintf ca sa scriu in shm_ptr;
	// shm_ptr[0] = 's';
	// shm_ptr[1] = 'r';
	// shm_ptr[3] = 'd';
	// // shm_ptr = result;
	// printf("Shm ptr: %s\n", (char *)shm_ptr);

	munmap(shm_ptr, getpagesize());


	printf("SHM PTR:%s\n", shm_ptr);

	// printf("I'm done");
	return 0;
}