#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>



int main() {
	char *argv[] = {"ls", NULL};

	pid_t pid = fork();

	if(pid<0) {
		return errno;
	} else if(pid==0) {
		printf("My PID=%d Child PID=%d\n", getppid(), getpid());
		execve("/bin/ls", argv, NULL);
		perror(NULL);
		printf("Parent %d Me %d\n", getppid(), getpid());

	} else {
		pid_t child_pid = wait(NULL);
		printf("Child %d finished\n", child_pid);
	}

	return 0;
}