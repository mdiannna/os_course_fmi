#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>



#define MAX_BYTES 100

int main (int argc, char const * argv[]) {
	if (argc != 3) {
		printf("Wrong number of parameters. Should be 3. Program exited.\n");
		return 0;
	}

	// Read file names from console arguments
	char const * filename1 = argv[1];
	char const * filename2 = argv[2];

   	char * buf = malloc(MAX_BYTES * sizeof(char));

   	// Open files and obtain file descriptors
 	int fd_read = open(filename1,  O_RDONLY, S_IRUSR);	
 	int fd_write = open(filename2,  O_WRONLY|O_CREAT, S_IRWXU| S_IROTH );	


 	// Atentie la conditiile while si initializarea variabilelor!!!
	size_t readBytes = 1;
	size_t writtenBytes = 0;
	
	while (readBytes != 0) {
 		// Atentie la conditiile while si initializarea variabilelor!!!
  		readBytes = read(fd_read, buf, MAX_BYTES);
  		writtenBytes = 0;

  		// Daca readBytes < 0 => a aparut o eroare
  		if (readBytes < 0) {
  			perror("Read buffer error");
  			return errno;
  		}

		
		while(writtenBytes < readBytes) {
			writtenBytes = write(fd_write, buf, readBytes);

			if (writtenBytes < 0) {
	  			perror("Error writting bytes");
	  			return errno;
	  		}	
		}

	}

	// Atentie!!! de inchis fisierele!!! 
	close(fd_read);
	close(fd_write);]



	return 0;
}
