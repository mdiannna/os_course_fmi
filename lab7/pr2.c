#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <pthread.h>
#include <semaphore.h>


sem_t sem;
pthread_mutex_t mtx;

// Total number of threads
int NTHRS;
int current_threads_reached;

void barrier_point() {
	pthread_mutex_lock(&mtx);
	current_threads_reached++;
	pthread_mutex_unlock(&mtx);

	// busy wait
	while(current_threads_reached < NTHRS);
}


void * tfun(void * v) {
	int *tid = (int *) v;

	if (sem_wait(&sem)) {
		perror(NULL);
		return NULL;
		// return errno;
	}

	printf("%d reached the barrier\n", *tid);
	barrier_point();

	if (sem_post(&sem)) {
		perror(NULL);
		// return errno;
		return NULL;
	}

	printf("%d passed the barrier\n", *tid);

	free(tid);
	return NULL;
}


void init(int N) {
	NTHRS = N;	
}


int main() {

	int S;
	int N = 5;

	// Initializarea semaforului
	if (sem_init(&sem, 0, S)) {
		perror(NULL);
		return errno;
	}

	/* Init mutex */
	if (pthread_mutex_init(&mtx, NULL)) {
		perror(NULL);
		return errno;
	}

	// Init N
	init(N);

	pthread_t thr[NTHRS];


	for (int i=0; i<NTHRS; i++) {
		int * count = (int *) malloc(sizeof(int));
		*count = i;
		if (pthread_create(&thr[i], NULL, tfun, (void *) count)) {
			perror(NULL);
			return errno;
		}	
	}


	for (int i=0; i<NTHRS; i++) {
		if (pthread_join(thr[i], NULL)) {
			perror(NULL);
			return  errno;
		}
	}

	sem_destroy(&sem);
	return 0;
}