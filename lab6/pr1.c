#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <errno.h>

void * hello(void * v) {
	char * in = (char *) v;
	int len = strlen(in);
	char* out = (char *) malloc(len*sizeof(char));
	for(int i=0; i<len; i++) {
		out[len-i-1] = in[i];
	}

	return out;
}

int main() {
	pthread_t thr;
	if (pthread_create(&thr, NULL, hello, "world!")) {
		perror(NULL);
		return errno;
	}

	void * result;

	if(pthread_join(thr, &result)) {
		perror(NULL);
		return errno;
	}

	printf("result:\n%s\n", (char*) result );
	
	return 0;
}