#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <errno.h>


int A[1000][1000];
int B[1000][1000];
int Am, Ap, Bp, Bn;

void citire() {
	FILE *f;
	f = fopen("date.in", "r");
	// Matricea A
	fscanf(f, "%d %d", &Am, &Ap);
	for (int i=0; i<Am; i++) {
		for (int j=0; j<Ap; j++) {
			fscanf(f, "%d", &A[i][j]);
		}
	}
	// Matricea B
	fscanf(f, "%d %d", &Bp, &Bn);
	for (int i=0; i<Bp; i++) {
		for (int j=0; j<Bn; j++) {
			fscanf(f, "%d", &B[i][j]);
		}
	}

	if(Ap!=Bp) {
		printf("Matricile nu pot fi inmultite\n");
		return;
	}
}

void afis() {
	printf("Matricea A:\n");

	for (int i=0; i<Am; i++) {
		for (int j=0; j<Ap; j++) {
			printf("%d ", A[i][j]);
		}
		printf("\n");
	}
	
	printf("Matricea B:\n");

	for (int i=0; i<Bp; i++) {
		for (int j=0; j<Bn; j++) {
			printf("%d ", B[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}



void * calcElement(void * v) {
	

	// char * elem = (char *) v;
	int * elem = (int *) v;
	// printf("%d ", elem[0]);
	// printf("%d  \n", elem[1]);
	char * returnResult = (char*) malloc(5*sizeof(char));

	int result = 0;
	int i = elem[0];
	int j = elem[1];

	for (int k=0; k<Ap; k++) {
		result += A[i][k] * B[k][j];
		// printf("%d ", A[i][k]);
		// printf("%d   ", B[k][j]);
	}

	 snprintf(returnResult, 5, "%d", result);
	// printf("Thread result: %s\n", returnResult);

	return returnResult;
}

int main() {
	citire();
	afis();

	pthread_t thr[20];
	int thread_cnt = 0;


	for (int i=0; i<Am; i++) {
		for (int j=0; j<Bn; j++) {
				int * arg = (int *) malloc(2*sizeof(int));

				arg[0] = i;
				arg[1] = j;
				
				if (pthread_create(&thr[thread_cnt], NULL, calcElement, arg)) {
					perror(NULL);
					return errno;
				}
				printf("----Thread %d started----\n", thread_cnt);
				thread_cnt++;
		}
	}

	for (int i=0; i<thread_cnt; i++) {
		void * result;

		if(pthread_join(thr[i], &result)) {
			perror(NULL);
			return errno;
		}
		printf("FINAL result thread %d:\n%s\n", i, (char*) result );
		printf("\n");
	}
	

	return 0;
}