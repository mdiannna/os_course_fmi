#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

int toInt(char * number) {
	int result = 0;
	int i=0;
	for(i=0; i<strlen(number); i++ ){
		 result = result*10 + (number[i]-'0');
	}
	return result;
}

int main(int argc, char *argv[]) {
	if(argc < 2) {
		printf("Error! Wrong number of parameters. Exit\n");
		return 0;
	}

	printf("Starting parent %d\n",getpid());

	char * input_nr_char;
	pid_t pid;

	for(int i=0; i<argc-1; i++) {

		input_nr_char = argv[i+1];
		char *childargv[] = {"", input_nr_char, NULL};

 		pid = fork();

 		if(pid<0) {
			return errno;
		} else if(pid==0) {
			execve("./calculate", childargv, NULL);
			perror(NULL);
			return errno;
		} 
	}


	for(int i=0; i<argc-1; i++) {
		pid_t child_pid = wait(NULL);
		if(child_pid < 0){
			perror(NULL);
		}
		else {
			printf("Done parent %d Me %d \n", getpid(), child_pid);
		}
	}	
	

	return 0;
}