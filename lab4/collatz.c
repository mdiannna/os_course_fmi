#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
#include <string.h>

int toInt(char * number) {
	int result = 0;
	int i=0;
	for(i=0; i<strlen(number); i++ ){
		 result = result*10 + (number[i]-'0');
	}
	return result;
}

int main(int argc, char *argv[]) {
	if(argc != 2) {
		printf("Error! Wrong number of parameters. Exit\n");
		return 0;
	}

	char * input_nr_char = argv[1];
	char *childargv[] = {"", input_nr_char, NULL};
	pid_t pid = fork();

	if(pid<0) {
		return errno;
	} else if(pid==0) {
		printf("My PID=%d Child PID=%d\n", getppid(), getpid());
		execve("./calculate", childargv, NULL);
		
		perror(NULL);
		printf("Parent %d Me %d\n", getppid(), getpid());

	} else {
		pid_t child_pid = wait(NULL);
		printf("Child %d finished\n", child_pid);
	}

	return 0;
}