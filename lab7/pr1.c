#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <pthread.h>

#define MAX_RESOURCES  5

int available_resources = MAX_RESOURCES;
pthread_mutex_t mtx;

int decrease_count(int count) {
	// mutex lock
	pthread_mutex_lock(&mtx);

	if (available_resources < count) {
		return -1;
	} else {

		available_resources -=count;
		printf("Got %d resources %d remaining \n", count, available_resources);

	}
	// mutex unlock
	pthread_mutex_unlock(&mtx);

	return 0;
	
}

int increase_count(int count) {
	// mutex lock
	pthread_mutex_lock(&mtx);

	// Critical section
	available_resources += count;
	printf("Released %d resources %d remaining \n", count, available_resources);


	// mutex unlock
	pthread_mutex_unlock(&mtx);
}

void * decrease_count_function(void * v) {
	int * count = (int*) v;
	if (!decrease_count(*count)) {
		increase_count(*count);
	} 
	return count;
}

int main () {
	/* Init mutex */
	if (pthread_mutex_init(&mtx, NULL)) {
		perror(NULL);
		return errno;
	}

	pthread_t thr[10];

		
		for (int i=0; i<5; i++) {
			int * count = (int *) malloc(sizeof(int));
			*count = i;

			if (pthread_create(&thr[i], NULL, decrease_count_function, (void *) count)) {
				perror(NULL);
				return errno;
			}	
		}
		


		for (int i=0; i<5; i++) {

			void * result;

			if (pthread_join(thr[i], &result)) {
				perror(NULL);
				return  errno;
			}

			// increase_count(*((int*) result));
		}
	pthread_mutex_destroy(&mtx);

	return 0;
}