#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

int toInt(char * number) {
	int result = 0;
	int i=0;
	for(i=0; i<strlen(number); i++ ){
		 result = result*10 + (number[i]-'0');
	}
	return result;
}

int main(int argc, char *argv[]) {
	if(argc < 2) {
		printf("Error! Wrong number of parameters. Exit\n");
		return 0;
	}

	printf("Starting parent %d\n",getpid());

	char * input_nr_char;
	pid_t pid[100];

	for(int i=0; i<argc-1; i++) {
 		pid[i] = fork();

		input_nr_char = argv[i+1];
		printf("Input nr:%s\n", input_nr_char);
		char *childargv[] = {"", input_nr_char, NULL};
		
 		if(pid[i]<0) {
			return errno;
		} else if(pid[i]==0) {
			printf("My PID=%d Child PID=%d\n", getppid(), getpid());
			execve("./calculate2", childargv, NULL);
			printf(".llllllllllllllllll\n");
			perror(NULL);
			printf("Parent %d Me %d\n", getppid(), getpid());

		} 
	}


	for(int i=0; i<argc-1; i++) {
		pid_t child_pid = wait(NULL);
		if(child_pid < 0){
			perror(NULL);
		}
		else {
			printf("Done parent %d Me %d \n", getpid(), child_pid);
		}
	}	
	

	return 0;
}