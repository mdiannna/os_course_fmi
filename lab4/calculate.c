#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
#include <string.h>


int toInt(char * number) {
	int result = 0;
	int i=0;
	for(i=0; i<strlen(number); i++ ){
		 result = result*10 + (number[i]-'0');
	}
	return result;
}

int main(int argc, char *argv[]) {
	// printf("Child execution:\n");
	// printf("argc: %d\n", argc);
	if(argc != 2) {
		printf("Error! Wrong number of parameters in calculate program. Exit\n");
		return 0;
	}
	int input_nr = toInt(argv[1]);
	printf("%d: %d ", input_nr, input_nr);
	while(input_nr>1) {
		if(input_nr % 2 ==0) {
			input_nr = input_nr/2;
		}
		else {
			input_nr = 3 * input_nr + 1;
		}
		printf("%d ", input_nr );
	}
	printf("\n");
	return 0;
}